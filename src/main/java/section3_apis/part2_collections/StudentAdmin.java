package section3_apis.part2_collections;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StudentAdmin extends StudentAdminDataReader{

    private List<Student> students = new ArrayList<>();
    private Map<String, Course> totalCollection = new HashMap<>();
    private HashMap<String, Double> studentForGrades = new HashMap<>();
    private HashMap<Student, Double> courseForGrades = new HashMap<>();

    public void addStudent(Student student){
        students.add(student);
        //System.out.println(students);
    }

    public void addCourseData(String courseId, int studId, double courseGrade){
        // check of een course bestaat zoniet maak hem en voeg hem toe aan de courseGrade
        //System.out.println(courseId + studId + courseGrade);
        if (totalCollection.containsKey(courseId)){
            totalCollection.get(courseId).putGrade(studId, courseGrade);
        } else {
            Course studGrade = new Course(courseId);
            studGrade.putGrade(studId, courseGrade);
            totalCollection.put(courseId, studGrade);
        }
        //coursesInfo.add(course);
    }

    /**
     * Returns the students that are present in the database.
     * If the wildcard is *, all students will be returned. Else,
     * a simple case insensitive substring match to both first name and family name will be performed.
     * @return students
     */

    public List<Student> getStudents(String wildcard) {
        if (wildcard == "*"){
            return students;
        } else {
            // make a new List
            List<Student> subStudents = new ArrayList<>();
            // compile the string that you want to check
            Pattern p = Pattern.compile(wildcard.toLowerCase());
            for(Student student1 : students){
                //System.out.println(student1.getFirstName());
                // check if the wildcard is inside the first or last name
                Matcher firstName = p.matcher(student1.getFirstName().toLowerCase());
                Matcher lastName = p.matcher(student1.getLastName().toLowerCase());
                // if there is a match add that student to a new list
                if (firstName.find() || lastName.find()){
                    subStudents.add(student1);
                    System.out.println(student1);
                }
            }
            return subStudents;
        }
    }

    /**
     * Returns the grade of a student for the given course
     * @param student
     * @param course
     * @return grade
     */
    public double getGrade(Student student, Course course) {
        System.out.println(student);
        return totalCollection.get(course.getCourseId()).grades.get(student.getStudentId());
    }

    /**
     * returns all grades for a student, as [key=CourseID]:[value=Grade] Map
     * @param student
     * @return grades
     */
    public Map<String, Double> getGradesForStudent(Student student) {
        for (String key: totalCollection.keySet()){
            //System.out.println(key);
            studentForGrades.put(key, totalCollection.get(key).grades.get(student.getStudentId()));

        }
        System.out.println(studentForGrades);
        return studentForGrades;
    }

    /**
     * Returns all grades for a course, as [key=Student]:[value=Grade] Map
     * @param course the course
     * @return grades
     */
    public Map<Student, Double> getGradesForCourse(Course course) {
        for (Student stud: students){
            System.out.println(stud);
            courseForGrades.put(stud, totalCollection.get(course.getCourseId()).grades.get(stud.getStudentId()));
        }
        return courseForGrades;
    }
}
