package section1_intro.part1_language_basics;

import java.text.NumberFormat;

public class GeometryAnalyser {
    public static void main(String[] args) {
        //your code here
        NumberFormat numberformat = NumberFormat.getIntegerInstance();
        numberformat.setMaximumFractionDigits(1);

        if (args[4].equals("dist")){
            Point p1 = new Point();
            p1.x = Integer.parseInt(args[0]);
            p1.y = Integer.parseInt(args[1]);

            Point p2 = new Point();
            p2.x = Integer.parseInt(args[2]);
            p2.y = Integer.parseInt(args[3]);

            double result = p2.euclideanDistanceTo(p1);
            System.out.println(numberformat.format(result));
        }
        else if (args[4].equals("surf")){
            Point p1 = new Point();
            p1.x = Integer.parseInt(args[0]);
            p1.y = Integer.parseInt(args[1]);

            Point p2 = new Point();
            p2.x = Integer.parseInt(args[2]);
            p2.y = Integer.parseInt(args[3]);

            Square square = new Square();
            square.upperLeft = p1;
            square.lowerRight = p2;

            int result = square.surface();
            System.out.println(result);
        }
    }

}
