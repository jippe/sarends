package section1_intro.part1_language_basics;

public class Square {
    Point upperLeft;
    Point lowerRight;

    /**
     * returns the surface defined by the rectangle with the given upper left and lower right corners.
     * It assumes two corners have been created already!
     * @return
     */
    int surface(){
        //calculate surface - can you implement this?
        int x = Math.abs(upperLeft.x - lowerRight.x);
        int y = Math.abs(upperLeft.y - lowerRight.y);
        return x * y;
    }
}
