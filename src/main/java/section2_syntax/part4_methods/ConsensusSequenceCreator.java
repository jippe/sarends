/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section2_syntax.part4_methods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class ConsensusSequenceCreator {
    private String[] sequences;
    private boolean iupac;
    private String[] ambiguities;
    private String consensus = "";
    private ArrayList<String> allPossibleCharacters;

    /**
     * testing main.
     * @param args 
     */
    public static void main(String[] args) {
        String[] sequences = new String[4];
        sequences[0] = "GAAT";
        sequences[1] = "GAAA";
        sequences[2] = "GATT";
        sequences[3] = "GAAC";
        
        ConsensusSequenceCreator csc = new ConsensusSequenceCreator();
        String consensus;
        consensus = csc.createConsensus(sequences, true);
        System.out.println("consensus = " + consensus);
        //consensus should equal "GAWH"
        consensus = csc.createConsensus(sequences, false);
        //consensus should equal "GA[A/T][A/T/C]"
        System.out.println("consensus = " + consensus);
    }

    /**
     * creates a consensus sequence from the given array of sequences.
     * @param sequences the sequences to scan for consensus
     * @param iupac flag to indicate IUPAC (true) or bracket notation (false)
     * @return consensus the consensus sequence
     */
    public String createConsensus(String[] sequences, boolean iupac) {
        //ArrayList<Character> allPossibleCharacters = new ArrayList<>();
        this.sequences = sequences;
        this.iupac = iupac;
        this.ambiguities = new String[sequences[0].length()];
        buildAmbiguities();
        buildConsensus();
        return this.consensus;
    }

    private void buildConsensus() {
        //YOUR CODE
        StringBuilder conSen = new StringBuilder();

        for (int i = 0; i < allPossibleCharacters.size(); i++){
            System.out.println(allPossibleCharacters.get(i));
            if (allPossibleCharacters.size() == 1);{

            }

        }

    }

    private void buildAmbiguities() {
        //YOUR CODE
        allPossibleCharacters = new ArrayList<>();
        for (int i =0; i < sequences.length; i++){
            Set<Character> chars = new HashSet<>();
            for (String seq: sequences){
                //System.out.println(seq.charAt(i));
                chars.add(seq.charAt(i));
            }
            allPossibleCharacters.add(chars.toString());
            //System.out.println(chars);
        }


    }

}
